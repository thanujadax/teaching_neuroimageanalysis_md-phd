from __future__ import with_statement
# Python import
import os, re, errno, string, shutil, ntpath, sys
import time
from sets import Set

# Java imports
from sets import Set
from java.util import ArrayList
from java.awt import Rectangle
from java.awt import Color
from java.awt.geom import AffineTransform
from jarray import zeros, array
from java.util import HashSet
from java.lang import Math

# Fiji imports
from ij import *
from ij import IJ
from ij import ImagePlus
from ij import WindowManager
from ij.gui import WaitForUserDialog
from fiji.tool import AbstractTool
from ij import IJ  
from ij.process import ImageStatistics as IS
from ij.process import ImageConverter
import ij.io.OpenDialog;
from ij.io import DirectoryChooser
from ij.gui import GenericDialog
from ij.io import FileSaver
from ij.plugin.filter import GaussianBlur as Blur
from ij.plugin.filter import Filters
from ij.process import ByteProcessor , FloatProcessor
from mpicbg.ij.clahe import Flat
from mpicbg.ij.plugin import NormalizeLocalContrast

# TrakEM imports
from ini.trakem2 import Project
from ini.trakem2.display import Patch
from ini.trakem2.utils import Utils
from java.awt.geom import AffineTransform
from java.util import HashSet
from mpicbg.trakem2.align import Align, AlignTask
from ini.trakem2.display import Display, Patch

def folderFromPath(path): #folders have an ending os.sep
	head, tail = ntpath.split(path)
	return head + os.sep
	
def nameFromPath(path):
	head, tail = ntpath.split(path)
	return os.path.splitext(tail)[0]

def folderNameFromFolderPath(path):
	head, tail = ntpath.split(path)
	head, tail = ntpath.split(head)	
	return tail	
	
def mkdir_p(path):
	try:
		os.mkdir(path)
		IJ.log('Folder created: ' + path)
	except Exception, e:
		if e[0] == 20047:
			# IJ.log('Nothing done: folder already existing: ' + path)
			pass
		else:
			IJ.log('Exception during folder creation :' + str(e))
			raise
	return path

def promptDir(text):
	folder = DirectoryChooser(text).getDirectory()
	content = naturalSort(os.listdir(folder))
	IJ.log('Prompted for ' + text)
	IJ.log('Selected folder :'  + folder)
	return folder, content

def makeNeighborFolder(folder, name):
	neighborFolder = folderFromPath(folder.rstrip(os.sep)) + name + os.sep
	mkdir_p(neighborFolder)
	IJ.log('NeighborFolder created: ' + neighborFolder)
	return neighborFolder
	
def getPath(text):
	path = IJ.getFilePath(text)
	IJ.log('File selected: ' + path)
	return path
	
def naturalSort(l): 
    convert = lambda text: int(text) if text.isdigit() else text.lower() 
    alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ] 
    return sorted(l, key = alphanum_key)

def getName(text):  
	gd = GenericDialog(text)  
	gd.addStringField(text, 'Untitled')  
	gd.showDialog()  
	if gd.wasCanceled():  
		print 'User canceled dialog!'
		return
	return gd.getNextString()
  
def getNumber(text, default = 0, decimals = 3):  
	gd = GenericDialog(text)  
	gd.addNumericField(text, default, decimals)  # show 6 decimals
	gd.showDialog()  
	if gd.wasCanceled():  
		IJ.log('User canceled dialog!')  
		return  
	return gd.getNextNumber()

def getNamesFromFolderExt(folder, extension = '.tif'):
	list = os.listdir(folder)
	list = filter(lambda x: os.path.splitext(x)[1] == extension, list)
	list = naturalSort(list)
	# for i, name in enumerate(list):
		# list[i] = folder + name
	return list

def getMinMaxFor8Bit(minMaxs):
	gd = GenericDialog('Enter the min and max for each channel for the 8 bit transformation')
	for channel in minMaxs.keys():
		gd.addNumericField('Min ' + channel , minMaxs[channel][0], 2) # show 2 decimals  
		gd.addNumericField('Max ' + channel, minMaxs[channel][1], 2) 
	gd.showDialog()
	if gd.wasCanceled():
		IJ.log('User canceled dialog!')  
		return
	for channel in minMaxs.keys():
		minMaxs[channel][0] = gd.getNextNumber()
		minMaxs[channel][1] = gd.getNextNumber()
	return minMaxs

def readWriteXMLTransform(projectPath,layerIndex,folder):
	'''
	1-Take the TrakEM project 'projectPath'
	2-Read the transformation of the first patch of layer 'layerIndex'
	3-Read the locations of the first patch of layer 'layerIndex' and layer '1-layerIndex'. This is used to calculate the offset of the EM and LM images in the initial transformation file
	4-Write as a simple text file in folder + title of the project + InitialTransform.txt
	'''
	project = Project.openFSProject(projectPath, False)
	layerset = project.getRootLayerSet()
	layers = layerset.getLayers()
	layer = layerset.getLayers().get(layerIndex)
	patches = layer.getDisplayables(Patch)
	t =  patches.get(0).getAffineTransform()
	
	transPath = folder + project.getTitle() + 'InitialTransform.txt.txt'
	f = open(transPath,'w')
	f.write( str(t.getScaleX()) + "\n")
	f.write( str(t.getShearY())+ "\n")
	f.write( str(t.getShearX())+ "\n")
	f.write( str(t.getScaleY())+ "\n")
	f.write( str(t.getTranslateX())+ "\n")
	f.write( str(t.getTranslateY())+ "\n")
	
	f.write ( str( layers.get(layerIndex).getDisplayables(Patch).get(0).getX() ) + '\n')
	f.write ( str( layers.get(layerIndex).getDisplayables(Patch).get(0).getY() ) + '\n')
	f.write ( str( layers.get(1-layerIndex).getDisplayables(Patch).get(0).getX() ) + '\n')
	f.write ( str( layers.get(1-layerIndex).getDisplayables(Patch).get(0).getY() ) + '\n')
	f.close()
	IJ.log('Transformation saved in: ' + transPath)
	# read the parameters of the transformations
	trans = []
	f = open(transPath,'r')
	while 1:
		line = f.readline()
		if not line: break
		IJ.log(line)
		trans.append(float(line))
	f.close
	IJ.log('Transformation: ' + str(trans))
	closeProject(project)
	return trans

def writeAffineTransforms(project,path):
	with open(path,'w') as f:
		layerset = project.getRootLayerSet()
		for k,layer in enumerate(layerset.getLayers()):
			patch = layer.getDisplayables(Patch).get(0)
			t = patch.getAffineTransform()
			f.write( str(t.getScaleX()) + "\n")
			f.write( str(t.getShearY())+ "\n")
			f.write( str(t.getShearX())+ "\n")
			f.write( str(t.getScaleY())+ "\n")
			f.write( str(t.getTranslateX())+ "\n")
			f.write( str(t.getTranslateY())+ "\n")
	return

def writeAllAffineTransforms(project,path):
	layerset = project.getRootLayerSet()
	loader = project.getLoader()
	with open(path,'w') as f:
		for l,layer in enumerate(layerset.getLayers()):
			for patch in layer.getDisplayables(Patch):
				f.write(os.path.normpath(loader.getAbsolutePath(patch)) + '\n')
				f.write(str(l) + '\n')
				t = patch.getAffineTransform()
				f.write( str(t.getScaleX()) + '\n')
				f.write( str(t.getShearY())+ '\n')
				f.write( str(t.getShearX())+ '\n')
				f.write( str(t.getScaleY())+ '\n')
				f.write( str(t.getTranslateX())+ '\n')
				f.write( str(t.getTranslateY())+ '\n')
	IJ.log('All affine Transforms saved in: ' + path)
	return
	
def readTransform(path):
	IJ.log('Reading transformation file: ' + path)
	trans = []
	f = open(path,'r')
	while 1:
		line = f.readline()
		if not line: break
		IJ.log(line)
		try:
			line = float(line)
		except Exception, e:
			pass
		trans.append(float(line))
	f.close
	return trans

def readCoordinates(folder,tags):
	content = os.listdir(folder)
	for i in content:
		if (all(map(lambda x:x in i,tags)) == True):
			path = folder + i
			IJ.log('This file matched the tag --' + str(tags) + '-- in the folder ' + folder + ' : ' + path)
	f = open(path,'r')
	x = []
	y = []
	for i, line in enumerate(f):
		x.append(int(line.split("\t")[0]))
		y.append(int(line.split("\t")[1]))
	#x,y = map(lambda u: np.array(u),[x,y]) #fiji version, no numpy
	f.close()
	IJ.log('x = ' + str(x))
	IJ.log('y = ' + str(y))
	return x,y

def writeRectangle(rectangle,path):
	f = open(path,'w')
	f.write(str(rectangle.x) + '\n') 
	f.write(str(rectangle.y) + '\n')
	f.write(str(rectangle.width) + '\n')
	f.write(str(rectangle.height) + '\n')
	f.close()

def readRectangle(path):
	f = open(path,'r')
	res = []
	for i, line in enumerate(f):
		res.append(int(line))
	f.close()
	return Rectangle(res[0],res[1],res[2],res[3])
	
def crop(im,roi):
	ip = im.getProcessor()
	ip.setRoi(roi)
	im = ImagePlus(im.getTitle() + '_Cropped', ip.crop())
	return im

def localContrast(im):
	ipMaskCLAHE = ByteProcessor(im.getWidth(),im.getHeight())
	ipMaskCLAHE.threshold(-1)
	bitDepth = im.getBitDepth()
	if bitDepth == 8:
		maxDisp = Math.pow(2,8) - 1
	else:
		maxDisp = Math.pow(2,12) - 1
	ip = im.getProcessor()
	ip.setMinAndMax(0,maxDisp)
	Flat.getFastInstance().run(im, 127, 256, 3, ipMaskCLAHE, False)
	# ip = im.getProcessor()
	# ip.setMinAndMax(0,maxDisp)
	return im

def edges(im):
	filter = Filters()
	ip = im.getProcessor()
	filter.setup('edge',im)
	filter.run(ip)
	im = ImagePlus(os.path.splitext(im.getTitle())[0] + '_Edges',ip)
	return im

def blur(im,sigma):
	blur = Blur()
	ip = im.getProcessor()
	blur.blurGaussian(ip,sigma,sigma,0.0005)
	im = ImagePlus(os.path.splitext(im.getTitle())[0] + '_Blur',ip)
	return im

def normLocalContrast(im):
	NormalizeLocalContrast().run(im.getProcessor(), 30,30,3,True,True ) # something like repaint needed ?
	return im
	
def resize(im,factor):
	ip = im.getProcessor()
	ip = ip.resize(int(Math.floor(im.width * factor)))
	im = ImagePlus(im.getTitle().replace('.tif', '') + 'Resampled',ip)
	im.close()
	return im
	
def initSession(path = None, text = 'Select the session directory'):
	if path == None:
		folderSession, folderSessionContent = promptDir(text)
	else:
		folderSession, folderSessionContent = [path, naturalSort(os.listdir(path))]	
	session = ntpath.split(os.path.normpath(folderSession))[1]
	channels = filter(lambda x:os.path.isdir(folderSession + os.sep + x),folderSessionContent)
	nLayers = len( filter(lambda x: os.path.splitext(x)[1]=='.tif' ,os.listdir(folderSession + channels[0] + os.sep))  )
	IJ.log('Session initialized')
	IJ.log('Folder: ' + folderSession)
	IJ.log('Session name: ' + str(session))
	IJ.log('Channels: ' + str(channels))
	IJ.log('Number of Layers: ' + str(nLayers))
	return folderSession, folderSessionContent, session, channels, nLayers

def initSessionFromFolder(folderSession):
	folderSessionContent  =  naturalSort(os.listdir(folderSession))
	session = ntpath.split(os.path.normpath(folderSession))[1]
	channels = filter(lambda x:os.path.isdir(folderSession + os.sep + x),folderSessionContent)
	nLayers = len( filter(lambda x: os.path.splitext(x)[1]=='.tif' ,os.listdir(folderSession + channels[0] + os.sep))  )
	IJ.log('Session initialized')
	IJ.log('Folder: ' + folderSession)
	IJ.log('Session name: ' + str(session))
	IJ.log('Channels: ' + str(channels))
	IJ.log('Number of Layers: ' + str(nLayers))
	return folderSession, folderSessionContent, session, channels, nLayers

def getRefChannel(channels, text = 'Reference Channel'): #dialog prompt to choose the reference channel
	gd = GenericDialog(text)
	gd.addChoice("output as", channels, channels[0])  
	gd.showDialog()  
	if gd.wasCanceled():  
		print "User canceled dialog!"  
		return  
	return gd.getNextChoice()

def getAllChannelPaths(im, folderSession, folderSessionContent, session, channels):
	paths = []
	nameIm = im.getOriginalFileInfo().fileName
	folderIm = im.getOriginalFileInfo().directory
	currentChannel = folderNameFromFolderPath(folderIm)


	for channel in channels:
		nameImChannel = nameIm.replace(currentChannel, channel)
		path = folderSession + channel + os.sep + nameImChannel
		paths.append(path)
	return paths

def initTrakem(path,nbLayers, mipmaps = False): #initialize a project
	project = Project.newFSProject("blank", None, path)
	project.getLoader().setMipMapsRegeneration(mipmaps)
	layerset = project.getRootLayerSet()
	for i in range(nbLayers): # create the layers
		layerset.getLayer(i, 1, True)
	project.getLayerTree().updateList(layerset) #update the LayerTree
	Display.updateLayerScroller(layerset) # update the display slider
	IJ.log('TrakEM project initialized with ' + str(nbLayers) + ' layers and stored in ' + path)
	return project
	
def exportFlat(project,outputFolder,scaleFactor, baseName = '', bitDepth = 8, layers = []):
	layerset = project.getRootLayerSet()
	loader = project.getLoader()
	for l,layer in enumerate(layerset.getLayers()):
		if (layers ==[] ) or (l in layers):
			IJ.log('Exporting layer ' + str(l))
			if bitDepth == 8:
				imp = loader.getFlatImage(layer,layerset.get2DBounds(),scaleFactor, 0x7fffffff, ImagePlus.GRAY8, Patch, layer.getAll(Patch), True, Color.black, None)
			elif bitDepth == 16:
				imp = loader.getFlatImage(layer,layerset.get2DBounds(),scaleFactor, 0x7fffffff, ImagePlus.GRAY16, Patch, layer.getAll(Patch), True, Color.black, None)		
			savePath = outputFolder + os.sep + baseName + '_' + str(l) + '.tif' #use the name of the outputFolder to name the images
			IJ.save(imp, savePath) 
			IJ.log('Layer ' + str(l) +' flat exported to ' + savePath)
			imp.close()

def exportFlatForPresentations(project,outputFolder,scaleFactor,rectangle):
	layerset = project.getRootLayerSet()
	loader = project.getLoader()
	for l,layer in enumerate(layerset.getLayers()): # import the patches 
		IJ.log('Exporting layer ' + str(l) + 'with rectangle ' + str(rectangle) + 'scale factor ' + str(scaleFactor))
		imp = loader.getFlatImage(layer,rectangle,scaleFactor, 0x7fffffff, ImagePlus.GRAY8, Patch, layer.getAll(Patch), True, Color.black, None)
		IJ.save(imp,outputFolder + os.sep + nameFromPath(outputFolder.rstrip(os.sep)) + '_' + str(l) + '.tif') #use the name of the outputFolder to name the images
		IJ.log('Layer ' + str(l)+' flat exported to ' + outputFolder + os.sep + nameFromPath(outputFolder.rstrip(os.sep)) + '_' + str(l) + '.tif')
		imp.close()

def exportFlatCloseFiji(project,outputFolder,scaleFactor):
	#todo: check whether the output file already exists. If yes, skip
	for l,layer in enumerate(layerset.getLayers()): 
		savePath = outputFolder + os.sep + nameFromPath(outputFolder.rstrip(os.sep)) + '_' + str(l) + '.tif' 
		savePathNext = outputFolder + os.sep + nameFromPath(outputFolder.rstrip(os.sep)) + '_' + str(l+1) + '.tif'
		if os.isfile(savePathNext):
			IJ.log('Skipping layer ' + str(l) + ': already processed')
		else:
			IJ.log('Exporting layer ' + str(layer) + '; layer number ' + str(l))
		layerset = project.getRootLayerSet()
		loader = project.getLoader()
		imp = loader.getFlatImage(layer,layerset.get2DBounds(),scaleFactor, 0x7fffffff, ImagePlus.GRAY8, Patch, layer.getAll(Patch), True, Color.black, None)
		IJ.save(imp, savePath)
		IJ.log('Layer ' + str(layerCurrent)+' flat exported to ' + savePath)
		imp.close()
	IJ.log('exportFlatCloseFiji has reached the end')

def exportFlatRoi(project, scaleFactor, x, y, w, h, layer, saveName):	
	loader = project.getLoader()
	rectangle = Rectangle(x-int(w/2),y-int(h/2),w,h)
	patches = layer.find(Patch, x, y)
	print patches
	# IJ.log('patches' + str(patches))
	
	for p, patch in enumerate(patches):
		visible = patch.visible
		patch.visible = True
		tiles = ArrayList()
		tiles.add(patch)
		print 'tiles',tiles
		print 'rectangle',rectangle
		IJ.log('Patch ' + str(patch) + ' cropped with rectangle ' + str(rectangle) )
		imp = loader.getFlatImage(layer, rectangle, scaleFactor, 0x7fffffff, ImagePlus.GRAY8, Patch, tiles, True, Color.black, None)
		exportName = saveName + '_' + str(int(p)) + '.tif'
		IJ.save(imp, exportName)
		patch.visible = visible
	
def readWriteCurrentIndex(outputFolder,text):
	layerFile = outputFolder + os.sep + 'currentNumber_' + text + '.txt'
	if 	os.path.isfile(layerFile):
		f = open(layerFile,'r')
		layerCurrent = int(f.readline())
		f.close()
	else:
		layerCurrent = 1
	f = open(layerFile,'w')
	f.write(str(layerCurrent + 1))
	f.close()
	IJ.log('Current index called from ' + layerFile + ' : ' + str(layerCurrent))
	return layerCurrent

def closeProject(project):
	project.getLoader().setChanged(False) #no dialog if there are changes
	project.destroy()

def to8Bit(*args):
	im = args[0]
	if len(args)==1:
		min = 0
		max = 4095
	else:
		min = args[1]
		max = args[2]
	ip = im.getProcessor()
	ip.setMinAndMax(min,max)
	IJ.run(im, "8-bit", "")
	return im

def saveLog(path):
	logWindows = WindowManager.getWindow('Log')
	textPanel = logWindows.getTextPanel()
	theLogText =  textPanel.getText().encode('utf-8')
	with open(path,'a') as f:
		f.write('The log has been saved at this time: ' + time.strftime('%Y%m%d-%H%M%S') + '\n')
		f.write(theLogText)
	logWindows.close()
	return

def findFilesFromTags(folder,tags):
	IJ.log('Looking for files in ' + folder + ' that match the following tags: ' + str(tags))
	filePaths = []
	for (dirpath, dirnames, filenames) in os.walk(folder):
		for filename in filenames:
			if (all(map(lambda x:x in filename,tags)) == True):
				path = os.path.join(dirpath, filename)
				filePaths.append(path)
				IJ.log('Found this file: ' + path)
	return filePaths

def readSessionMetadata(folder):
	IJ.log('b')
	sessionMetadataPath = findFilesFromTags(folder,['session','metadata'])[0]
	IJ.log('c')
	with open(sessionMetadataPath,'r') as f:
		lines = f.readlines()
		IJ.log(str(lines[1].replace('\n','').split('\t')))
		IJ.log(str(len(lines[1].replace('\n','').split('\t'))))
		width, height, nChannels, xGrid, yGrid, scaleX, scaleY = lines[1].replace('\n','').split('\t')
		channels = []
		IJ.log(str(lines))
		IJ.log(nChannels)
		for i in range(int(nChannels)):
			IJ.log(str(i))
			l = lines[3+i]
			IJ.log(str(l))
			l = l.replace('\n','')
			IJ.log(str(l))
			l = l.split('\t')
			IJ.log(str(l))
			channels.append(l[0])
	return int(width), int(height), int(nChannels), int(xGrid), int(yGrid), float(scaleX), float(scaleY), channels
	
def readParameters(path):
	d = {}
	with open(path,'r') as f:
		lines = f.readlines()
	for line in lines:
		if line[0] != '#':
			line = line.split('=')
			value = line[1].replace(' ','')
			key = line[0].replace(' ','')
			try:
				if '.' in value:
					value = float(value)
				else:
					value = int(value)
			except Exception, e:
				pass
			d[key] = value
	return d
	
def startPlugin(namePlugin):
	IJ.log('Plugin ' + namePlugin + ' started at ' + str(time.strftime('%Y%m%d-%H%M%S')))
	externalArguments = Macro.getOptions().replace('"','').replace(' ','')
	return externalArguments

def terminatePlugin(namePlugin, logPath):
	IJ.log('Plugin ' + namePlugin + ' terminated at '  + str(time.strftime('%Y%m%d-%H%M%S')))
	saveLog(logPath)
	IJ.run('Quit')
	
def shouldRunAgain(namePlugin, l, nLayers, logPath, project):
	if l + 1 < nLayers:
		IJ.log('Plugin ' + namePlugin + ' still running at '  + str(time.strftime('%Y%m%d-%H%M%S')) + '. Still ' + str(nLayers - l) + ' layers to go' )
		saveLog(logPath)
		time.sleep(3)
		closeProject(project)
		time.sleep(1)
		sys.exit(2)
		terminatePlugin(namePlugin, logPath)
	else:
		IJ.log(namePlugin + ' done.')
		time.sleep(2)
		closeProject(project)
		sys.exit(0)
		terminatePlugin(namePlugin, logPath)

def incrementCounter(path):
	l=''
	if not os.path.isfile(path):
		l = 0
	else:
		with open(path, 'r') as f:
			l = int(f.readline())
	with open(path, 'w') as f:
		f.write(str(l+1))
	return l
