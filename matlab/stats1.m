% MD - PhD course cluster analysis

lysosomeFileName = '/home/thanuja/Dropbox/MD-PhD_Course_2015/test/Lysosomecomparisons.xlsx';
% read excel file
lysosomeData = xlsread(lysosomeFileName,'Sheet1');
% plot scatter plot
channel1 = lysosomeData(:,2);
channel2 = lysosomeData(:,3);
% identify two clusters (classifiy the lysosomes)
figure;scatter(channel1,channel2);
title('Scatter plot of all lysosomes')
xlabel('Channel 1: Red')
ylabel('Channel 2: FITC')
% calculate the gradient of the lines (in scatter plot?)
% selecting points manually
% line 1 
line1point1x = 6;
line1point1y = 18;
line1point2x = 51;
line1point2y = 195;

x = [6 51];
y = [18 195];
grad1 = (y(2)-y(1))/(x(2)-x(1))

hold on
plot(x,y,'r','LineWidth',2);

% line 2
line1point1x = 30;
line1point1y = 8;
line1point2x = 227;
line1point2y = 72;

x = [30 227];
y = [8 72];

plot(x,y,'g','LineWidth',2);

grad2 = (y(2)-y(1))/(x(2)-x(1))
hold off


