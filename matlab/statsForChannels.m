% MD - PhD course cl

colID_channel1 = 2;
colID_channel2 = 3; 
colID_channelLabels = 4;
redChannelLabel = 1;
greenChannelLabel = 2;

lysosomeFileName = '/home/thanuja/Dropbox/MD-PhD_Course_2015/results/combined.xls';
lineInfoFileName = '/home/thanuja/Dropbox/MD-PhD_Course_2015/results/lineInfo.txt';
% read excel file
lysosomeData = xlsread(lysosomeFileName,'Sheet1');
% plot scatter plot
channel1 = lysosomeData(:,colID_channel1);
channel2 = lysosomeData(:,colID_channel2);

% figure;scatter(channel1,channel2)

% get channel labels
channelLabels = lysosomeData(:,colID_channelLabels);

rowIDs_redCells = selectCellsFromIDs(lysosomeData,colID_channelLabels,redChannelLabel);
rowIDs_greenCells = selectCellsFromIDs(lysosomeData,colID_channelLabels,greenChannelLabel);

redCells = lysosomeData(rowIDs_redCells,[colID_channel1 colID_channel2]);
figure;scatter(redCells(:,1),redCells(:,2),'r','filled')
axis = [0 255 0 255];
h1 = lsline;
set(h1(1),'color','r','LineWidth',1.5)
p1 = polyfit(redCells(:,1),redCells(:,2),1); 
hold on

greenCells = lysosomeData(rowIDs_greenCells,[colID_channel1 colID_channel2]);
scatter(greenCells(:,1),greenCells(:,2),'g','filled')
axis = [0 255 0 255];
h2 = lsline;
set(h2(1),'color','g','LineWidth',1.5)
p2 = polyfit(greenCells(:,1),greenCells(:,2),1);

%title('Scatter plot of all lysosomes','FontSize',38)
%xlabel('Channel 1 (Red) dominant','FontSize',35)
%ylabel('Channel 2 (FITC) dominant','FontSize',35)



hold off

% write to file
fileID = fopen(lineInfoFileName,'w');
fprintf(fileID,'line1: gradient = %d ; intercept = %d \n',p1(1),p1(2));
fprintf(fileID,'line2: gradient = %d ; intercept = %d ',p2(1),p2(2));
fclose(fileID);

% % ratio (ch2/ch1)
% rangeVector = -2:0.2:3;
% expRangeVector = exp(rangeVector);
% 
% ratioVector1 = channel1 ./ channel2;
% ratioVector1(ratioVector1>12) = 13;
% 
% figure; hist(ratioVector1,expRangeVector)
% title('Histogram Ch1/Ch2')
% 
% ratioVector2 = channel2 ./ channel1;
% ratioVector2(ratioVector2>12) = 13;
% figure; hist(ratioVector2,expRangeVector)
% title('Histogram Ch2/Ch1')
