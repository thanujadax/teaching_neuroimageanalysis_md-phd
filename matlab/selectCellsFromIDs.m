function rowIDs = selectCellsFromIDs(inputMat,colID,colVal)

rowIDs = find(inputMat(:,colID)==colVal);