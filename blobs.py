import os
import math

import ij
from ij import IJ
from ij.blob import Blob
from ij.blob import ManyBlobs
from collections import defaultdict
from java.util import HashSet
from ij import IJ, ImagePlus 
from ij.gui import GenericDialog

from sys import path
from java.lang.System import getProperty
 
# extend the search path by custom directory
fijiCommonPath = '/home/thanuja/projects/teaching/neuroImageAnalysis_MD-PhD';
path.append(fijiCommonPath)


import fijiCommon as fc

def pixelCoordinatesFromBlob(blob):
	blobContour = blob.outerContour
	coords = []
	blobBounds = blobContour.getBounds()
	for x in range(blobBounds.x - 1, blobBounds.x + blobBounds.width ):
		for y in range(blobBounds.y - 1, blobBounds.y + blobBounds.height ):
			if blobContour.contains(x, y) or blobContour.contains(x, y-1) :
				coords.append([x,y])
	return coords

def valuesFromCoordinates(coords, im):
	ip = im.getProcessor()
	values = []
	for coord in coords:
		values.append(ip.get(coord[0], coord[1]))
	return values

def getMean(a):
	return (sum(a)/len(a))	

def getTop20Mean(a):	

	#print "print entire array \n"
	#print ', '.join(map(str, a))
	
	channel1_sorted = []
	channel1_sorted = sorted(a)

	#print "print sorted \n"
	#print ', '.join(map(str, channel1_sorted))
	# get the top 20%
	numElements = len(channel1_sorted)
	numElementsToLook = int(math.ceil(len(channel1_sorted) * 0.2))
	startInd = numElements - numElementsToLook
	channel1_max20 = channel1_sorted[startInd:numElements]
	channel1_mean = getMean(channel1_max20)
	#print "mean: "
	#print str(channel1_mean)
	#print "******************************** \n"
	return channel1_mean

# im = IJ.getImage() # get the currently opened image. Could open files from a folder as well. See my fijiCommon library of custom functions to handle files and file prompts
string1 = "Please select binary map of blobs"
imageChannelPath =  fc.getPath(string1)
im = IJ.openImage(imageChannelPath)
ip = im.getProcessor()

blobs = ManyBlobs(im)
blobs.findConnectedComponents()
imLabeled = blobs.getLabeledImage()
ipLabeled = imLabeled.getProcessor()
ipLabel = ipLabeled.toFloat(2, None) # this is an imageprocessor that contains the locations of the blobs. Each blob has a different intensity value 

d={} # the dictionnary that will contain the [x,y] coordinates of each pixel of each blob. In the dictionnary, the key of each blob is the random value intensity that comes from ipLabel
for x in range(im.getWidth()):
	for y in range(im.getHeight()):
		key = ipLabel.getPixel(x,y)
		if key != 0 and ip.getPixel(x,y) == 0: #make sure that the pixel is black because IJBlob erroneously labels the contours of all blobs too
			if key in d:
				d[key].append([x,y])
			else:
				d[key] = [[x,y]]

print 'dictionnary',d

# taking a multicolor image, it should output a tab delimited text file with all intensity values of all blobs of all channels. It should be easily readable in excel (tab delimited for example)
# all pictures (channels) should be in a folder
# im = IJ.getImage()

#the fijicommon.py should be in the plugin directory
# path = fc.getPath("Path to the images from separate channels:")
# directory should not contain trailing slash
# direcotry = fc.promptDir("Directory containing the images from separate channels:")

channelImageProcList = []
channelImageList = []

columnLabels = ["blob_ID", "channel_1", "channel_2"]

outputFilePath = fc.getPath("Output file name: ")
outputFile = file(outputFilePath,"w")
# writet the header
headerString = "\t".join(columnLabels)
outputFile.write(headerString)
outputFile.write("\n")

for i in range(0,2) :
	# print 'Reading image', i
	string1 = "Please select image for channel %d : " % (i+1)
	imageChannelPath =  fc.getPath(string1)
	im = IJ.openImage(imageChannelPath)
	channelImageList.append(im)
	ip = im.getProcessor()
	channelImageProcList.append(ip)

# for each pixel in each blob, read the values from the 3 channels	
blobsList = d.keys()

for i in blobsList :
	listOfPixels = d.get(i)
	numPixels = len(listOfPixels)

	channelIntensity = [[] for _ in range(2)]
	outputList = []

	# for each pixel
	for j in range(0,numPixels) :
		# for each channel
		pixelIndex = listOfPixels[j]
		x = pixelIndex[0]
		y = pixelIndex[1]

		#outputList = []
		#outputList.append(str(i))
		#outputList.append(str(x))
		#outputList.append(str(y))
	
		for ch in range(0,2) :
			# get the intensity
			
			channelIntensity[ch].append(channelImageProcList[ch].getPixel(x,y))
			# print 'pixel intensity for channel: ', channelIntensity
			
			# append to output list to be written to file
			# outputList.append(str(channelIntensity))

			
			
		# write dictionary pixelChannelIntensities to a tab delimited file - for each pixel
		# outputString = "\t".join(outputList)
		# outputFile.write(outputString)
		# outputFile.write("\n")

	# channelIntensity contains the channel info both this blob, for both channels. get the mean of the max20%

	# print ', '.join(map(str, channelIntensity[0]))

	channel1_mean = getTop20Mean(channelIntensity[0])
	channel2_mean = getTop20Mean(channelIntensity[1])
	outputList.append(str(i))
	outputList.append(str(channel1_mean))
	outputList.append(str(channel2_mean))
	
	outputString = "\t".join(outputList)
	outputFile.write(outputString)
	
	outputFile.write("\n")
	
	

outputFile.close()



################################################
# get the path for the channels
# im = IJ.openImage(path)
# ip = im.getProcessor()
#pixInten = ip.getPixel(x,y)
# content = os.listdir(directory) # [im1.tif, im3.tif, im2.tif]
# sorted = fc.naturalSort(content) # sorted 



