***Neuroinformatics block course for MD - PhD students***

June 2015

***ijblob-1.3.jar***

Should be added to <FijiRoot>/jars

***fijiCommon.py***

Should be in the search path when running blobs.py. 
If blobs.py complains not finding it, there's code included in blobs.py to specify its location

***blobs.py***

Main file. 

It prompts to open the binary blob map (pix intensity of blobs should be zero) and each of the 2 channels separately. These should already be saved as separate images.

Finally, the output (mean of the max 20% of each channel) is written to a tab delimited file.

